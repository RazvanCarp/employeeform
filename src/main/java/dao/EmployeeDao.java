package dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Employee;

/**
 * DAO + methods
 */
@SuppressWarnings("unchecked")
public class EmployeeDao {
	@PersistenceContext
	EntityManager e;

	/**
	 * here we have all the employees
	 * 
	 * @return employees
	 */
	public List<Employee> getAllEmployees() {
		List<Employee> employees = e.createQuery("FROM Employee").getResultList();
		return employees;
	}

	/**
	 * Find employee using id
	 * 
	 * @param id
	 * @return employee
	 */
	public Employee getWithId(int id) {

		return e.find(Employee.class, id);
	}

	/**
	 * this method is used to add an employee
	 * 
	 * @param employee
	 */
	public void insert(Employee employee) {
		e.persist(employee);
	}

	/**
	 * this method is used to remove an employee
	 * 
	 * @param employee
	 */
	public void remove(Employee employee) {
		e.remove(employee);
	}

}
