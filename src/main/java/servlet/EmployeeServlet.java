package servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EmployeeDao;
import model.Employee;

@SuppressWarnings("serial")
@WebServlet(urlPatterns = "/Employee")
/**
 * Servlet (doGet and doPost). To show all the employees, we'll use GET
 */
public class EmployeeServlet extends HttpServlet {
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {

		EmployeeDao employeeDao = new EmployeeDao();
		List<Employee> employeeList = new ArrayList<Employee>();
		employeeList = employeeDao.getAllEmployees();
		request.setAttribute("employee", employeeList);

	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.sendRedirect(getServletContext().getContextPath() + "/");

	}

}
