package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Department class with set and get
 */
@Entity
@Table(name = "department")
public class Department {
	@Id
	private int id;
	@Column
	private String name;

	/**
	 * Constructor with parameters..
	 * 
	 * @param id
	 * @param name
	 */
	public Department(int id, String name) {
		this.id = id;
		this.name = name;
	}

	/**
	 * Constructor without parameters..
	 */
	public Department() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String deptName) {
		this.name = deptName;
	}
}