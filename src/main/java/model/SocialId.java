package model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * SocialId class with set and get.
 */
@Entity
@Table(name = "socialId")
public class SocialId {
	@Id
	private int id;
	@Column
	private String socialId;

	/**
	 * Constructor with parameters
	 * 
	 * @param id
	 * @param socialId
	 */
	public SocialId(int id, String socialId) {
		this.id = id;
		this.socialId = socialId;
	}

	/**
	 * Constructor without parameters
	 */
	public SocialId() {

	}

	public String getSocialId() {
		return socialId;
	}

	public void setSocialId(String socialId) {
		this.socialId = socialId;
	}
}