package model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * Employee class with relations between them. set and get
 */
@Entity
@Table(name = "employee")
public class Employee {
	@Id
	@GeneratedValue
	private int id;
	@Column
	private String name;

	@ManyToOne
	private Department department;

	@OneToOne
	private SocialId socialId;

	@ManyToMany(mappedBy = "employee")
	private Set<Project> projects;

	/**
	 * Constructor with parameters.
	 * 
	 * @param id
	 * @param name
	 * @param department
	 * @param socialId
	 * @param projects
	 */
	public Employee(int id, String name, Department department, SocialId socialId, Set<Project> projects) {
		super();
		this.id = id;
		this.name = name;
		this.setDepartment(department);
		this.setSocialId(socialId);
		this.setProjects(projects);
	}

	/**
	 * Constructor without parameters
	 */
	public Employee() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public SocialId getSocialId() {
		return socialId;
	}

	public void setSocialId(SocialId socialId) {
		this.socialId = socialId;
	}

	public Set<Project> getProjects() {
		return projects;
	}

	public void setProjects(Set<Project> projects) {
		this.projects = projects;
	}

}
